//Include Beverages
#include "Beverages/BeverageBase.hpp"
#include "Beverages/Coffee.hpp"
#include "Beverages/Espresso.hpp"

//Include Condiments
#include "Condiments/Caramel.hpp"
#include "Condiments/Cream.hpp"
#include "Condiments/Sugar.hpp"

//std::cout
#include <iostream>

//Function declaration
void Display(BeverageBase* aBeverage);

int main()
{
   //Beverage Pointer
   BeverageBase* beverage;

   //New beverage of derived class "Coffee"
   beverage = new Coffee();

   //Add Cream and Sugar to the coffee
   beverage = new Cream(beverage);
   beverage = new Sugar(beverage);

   //Display the description and cost of the beverage
   Display(beverage);

   system("pause");
   
   //Delete old beverage
   delete beverage;
   beverage = nullptr;

   //New beverage of derived class "Espresso"
   beverage = new Espresso();

   //Add Caramel to the espresso
   beverage = new Caramel(beverage);

   //Display the description and cost of the beverage
   Display(beverage);

   system("pause");
}

//Function implementation
void Display(BeverageBase* aBeverage)
{
   std::cout << "DESCRIPTION:\n" << aBeverage->Description() << std::endl;
   std::cout << "TOTAL COST: " << aBeverage->Cost() << std::endl;
}