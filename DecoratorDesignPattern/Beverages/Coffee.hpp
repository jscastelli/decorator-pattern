#ifndef COFFEE_HPP
#define COFFEE_HPP

#include "../Beverages/BeverageBase.hpp"

class Coffee : public BeverageBase
{
   public:

      virtual std::string Description() override;
      virtual double Cost() override;
};

#endif // !COFFEE_HPP