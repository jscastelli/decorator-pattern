#ifndef BEVERAGEBASE_H
#define BEVERAGEBASE_H

#include <string>

//Base class for which all beverages and condiments derive from.
//All derived classes need to override the Description and Cost methods
//for their specific implementation.
class BeverageBase
{
   public:

      virtual std::string Description() { return ""; };
      virtual double Cost() { return 0.0; };
};

#endif