#ifndef CREAM_HPP
#define CREAM_HPP

#include "CondimentBaseDecorator.h"

class Cream : public CondimentBaseDecorator
{
   //Constructor(s) & Public Member Functions
   public:

      Cream(BeverageBase* aDecoratedBeverage);

      virtual std::string Description() override;
      virtual double Cost() override;
};

#endif // !CREAM_HPP