#ifndef SUGAR_HPP
#define SUGAR_HPP

#include "CondimentBaseDecorator.h"

class Sugar : public CondimentBaseDecorator
{
   //Constructor(s) & Public Member Functions
   public:

      Sugar(BeverageBase* aDecoratedBeverage);

      virtual std::string Description() override;
      virtual double Cost() override;
};

#endif // !SUGAR_HPP