#include "Caramel.hpp"

Caramel::Caramel(BeverageBase* aDecoratedBeverage) : CondimentBaseDecorator(aDecoratedBeverage)
{}

std::string Caramel::Description()
{
   return mDecoratedBeverage->Description() + CondimentBaseDecorator::Description() + "Caramel\n";
}

double Caramel::Cost()
{
   return mDecoratedBeverage->Cost() + 0.75;
}
