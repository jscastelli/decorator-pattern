#ifndef CARAMEL_HPP
#define CARAMEL_HPP

#include "CondimentBaseDecorator.h"
class Caramel : public CondimentBaseDecorator
{
   //Constructor(s) & Public Member Functions
   public:

      Caramel(BeverageBase* aDecoratedBeverage);

      virtual std::string Description() override;
      virtual double Cost() override;
};

#endif // !CARAMEL_HPP