#include "CondimentBaseDecorator.h"

//Constructor
CondimentBaseDecorator::CondimentBaseDecorator(BeverageBase* aDecoratedBeverage)
{
   mDecoratedBeverage = aDecoratedBeverage;
}

//Derived classes will first call the base class Description() method before handling their own description.
std::string CondimentBaseDecorator::Description()
{
   return "   + ";
}
